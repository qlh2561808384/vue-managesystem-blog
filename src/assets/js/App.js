export default {
    SYS_NAME: "madison博客管理系统",
    /**
     * 左侧高度
     * @returns {number}
     */
    getLeftMenuHeight() {
        //浏览器打开高度
        let clientHeight = document.documentElement.clientHeight;
        //左侧高度
        return clientHeight - 60;
    },
    /**
     * 左侧高度
     * @returns {number}
     */
    getIfmAutoHeight() {
        //设置iframe自适高度
        return this.getLeftMenuHeight() - 65;
    },
    getSysName() {
        let sysNmae = this.SYS_NAME;
        return sysNmae;
    }
};
