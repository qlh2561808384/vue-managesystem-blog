import request from "../../http/request";
import el from "element-ui/src/locale/lang/el";

export default {
    USER_AUTH_KEY: "Authorization",
    //高级管理员的标识符，并不是改了这个就成为高级管理员了，仅仅是一个标识符。
    TABS_LIST: "tagList",
    ADMIN: "ADMIN",
    authObj: null,
    /**
     * 可以直接调用此方法获得角色名称
     * @returns {null}
     */
    getAuth() {
        return this._getVal("auth");
    },
    /**
     * 可以直接调用此方法获得当前token
     * @returns {null}
     */
    getToken() {
        return this._getVal("token");
    },
    /**
     * 可以直接调用此方法获得当前登录人
     * @returns {null}
     */
    getUserName() {
        return this._getVal("username");
    },

    /**
     * 可以直接调用此方法获得该用户的菜单
     * @returns {null}
     */
    getMenus() {
        return this._getVal("menus");
    },
    //获得组织ID
    getOrganizationId() {
        return this._getVal("organizationid");
    },

    //获得组织名称
    getOrganizationname() {
        return this._getVal("organizationname");
    },
    _getVal(val) {
        if (this.authObj == null) {
            //console.log("加载localStorage中的userAuth信息......");
            this._getAuth();
        }
        return this.authObj ? this.authObj[val] : null;
    },
    _getAuth() {
        let authString = window.localStorage.getItem(this.USER_AUTH_KEY);
        if (authString) {
            this.authObj = JSON.parse(authString);
            // console.log(authString);
        }
    },
    //以字符串返回，且转换了ADMIN为超级管理员。
    getAuthValueString() {
        let arr = this.getAuthValueArr();
        let str = arr.map(item => (item === this.ADMIN) ? "超级管理员" : item);
        return str.toString();
    },

    //以数组返回,且过滤了"ROLE_"
    getAuthValueArr() {
        let arr = this.getAuth();
        let str = [];
        arr.forEach(item => {
            str.push(item.authority.replace("ROLE_", ""));
        });
        return str;
    },
    /**
     * 保存用户基本权限信息
     * @param auth
     */
    setAuth(auth) {
        if (auth) {
            let authString = JSON.stringify(auth);
            window.localStorage.setItem(this.USER_AUTH_KEY, authString);
        }
    },
    /**
     * 清除localStorage中的userAuth
     */
    deleteAuth() {
        window.localStorage.removeItem(this.USER_AUTH_KEY);
        window.localStorage.removeItem(this.TABS_LIST);
    },
    /**
     * 进入登录页面
     * @param token
     */
    gotoLogin(token) {
        if (!token) {
            this.$router.push('/');
            // document.location.href = this._getProjectPath()+"/login.html";
        }
    }
}
