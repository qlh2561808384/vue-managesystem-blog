import http from './http';
import api from './api';
import da from "element-ui/src/locale/lang/da";

let auth = true;

export default {
  user: {
    register(data) {
      return http.post(api.User.register, JSON.stringify(data), auth);
    },
    getCaptcha(uuid) {
      // return http.get(api.User.getCaptcha, {uuid: uuid}, auth);
      return http.get(api.User.getCaptcha + '?uuid=' + uuid);
    },
    admin(data) {
      return http.post(api.User.admin, JSON.stringify(data), auth);
    },
    login(data) {
      return http.post(api.User.login, data, auth);
    },
    logout(){
      return http.get(api.User.logout);
    },
    getAllUser(page, limit, keyword){
      return http.get(api.User.getAllUSer, {page: page, limit: limit, keyword: keyword}, auth);
    },
    delUser(data) {
      return http.get(api.User.delUser + '/' + data);
    }
  },
  mock: {
    getTableData() {
      return http.get(api.mockData.TableData, auth);
    },
  },
  role: {
    getAllRole(page, limit, keyword) {
      // return http.get(api.role.getAllRole + "?page=" + page + "&limit=" + limit);
      return http.get(api.role.getAllRole, {page: page, limit: limit, keyword: keyword}, auth);
    },
    addRole(data){
      return http.post(api.role.addRole, JSON.stringify(data), auth);
    },
    delRole(data) {
      return http.get(api.role.delRole + '/' + data);
    },
    getPermissionList(roleId) {
      return http.get(api.role.getPermissionList + '/' + roleId);
    }
  },
  pay: {
    weChatPay: {
      weChatPay(status, data) {
        if (status !== "") {
          return http.post(api.pay.weChatPayOfficial);
        } else {
          // return http.post(api.pay.weChatPay);
          return http.post(api.pay.BinaryWeChatPay, data, auth);
        }
      },
      weChatPayQuery(data) {
        return http.get(api.pay.BinaryWeChatPayQuery, data, auth);
      }
    },
    aliPay: {
      aliPay(data) {
        return http.get(api.pay.aliPay, data, auth);
      },
      aliPayQuery(data) {
        return http.get(api.pay.aliPayQuery, data, auth);
      },
      aliPayRefund(data) {
        return http.post(api.pay.aliPayRefund, data, auth);
      },
      aliPayRefundQuery(data) {
        return http.get(api.pay.aliPayRefundQuery, data, auth);
      },
      aliPayClose(data) {
        return http.post(api.pay.aliPayClose, data, auth);
      },
      aliPayBillDownloadUrl(data) {
        return http.get(api.pay.aliPayBillDownloadUrl, data, auth);
      },
    },
    globalAliPay:{
      globalAliPay() {
        return http.get(api.pay.globalAliPay);
      }
    },
  },
  article:{
    uploadCoverImage(formData){
      return http.post(api.article.uploadCoverImage,formData);
    }
  },
  menu: {
    getMenuList() {
      return http.get(api.menu.getMenuList);
    },
    getMenuById(menuId) {
      return http.get(api.menu.getMenuById + '/' + menuId);
    },
    saveOrUpdateFrontMenu(data) {
      return http.post(api.menu.saveOrUpdateFrontMenu, data, auth);
    },
    deleteMenu(row){
      return http.post(api.menu.deleteMenu, row, auth);
    }
  },
  code: {
    getVerificationCode(phone){
      return http.get(api.code.getCode + "/" + phone);
    }
  }
}
