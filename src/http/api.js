/*
  统一管理存放后端提供接口
*/
import Goods from './api/goods.js';
const GLOBAL_API_URL = "http://localhost:8088";

export default {
  // 首页
  Index: {
    index: GLOBAL_API_URL + '/index/index'
  },

  // 个人中心
  Home: {
    UserInfo: GLOBAL_API_URL + '/user/info'
  },

  User: {
    register: GLOBAL_API_URL + '/user/register',
    getCaptcha: GLOBAL_API_URL + '/user/captcha.jpg',
    admin: GLOBAL_API_URL + '/user/admin',
    hello: GLOBAL_API_URL + '/user/hello',
    login: GLOBAL_API_URL + '/login',
    logout: GLOBAL_API_URL + '/logout',
    getAllUSer: GLOBAL_API_URL + "/user/getAllUser",
    delUser: GLOBAL_API_URL + "/user/delUser"
  },
  mockData: {
    TableData: '/home/getData'
  },
  role: {
    getAllRole: GLOBAL_API_URL + "/role/getAllRole",
    addRole: GLOBAL_API_URL + "/role/addRole",
    delRole: GLOBAL_API_URL + "/role/delRole",
    getPermissionList: GLOBAL_API_URL + '/role/roleToMenu'
  },
  pay: {
    weChatPay: GLOBAL_API_URL + "/wx/nativePay",
    weChatPayOfficial: GLOBAL_API_URL + "/wxPayOfficial/nativePay",
    BinaryWeChatPay: GLOBAL_API_URL + "/pay/createOrder",
    BinaryWeChatPayQuery: GLOBAL_API_URL + "/pay/queryOrder",

    // aliPay: GLOBAL_API_URL + '/alipay/pc/page/pay',
    aliPay: GLOBAL_API_URL + '/alipay/app/pay',
    aliPayQuery: GLOBAL_API_URL + '/alipay/assist/query',
    aliPayRefund: GLOBAL_API_URL + '/alipay/assist/refund',
    aliPayRefundQuery: GLOBAL_API_URL + '/alipay/assist/refund/query',
    aliPayClose: GLOBAL_API_URL + '/alipay/assist/close',
    aliPayBillDownloadUrl: GLOBAL_API_URL + '/alipay/assist/query/BillDownloadUrl',
    globalAliPay: GLOBAL_API_URL + "/globalAliPay/pc/page/pay"
  },
  article: {
    uploadCoverImage: GLOBAL_API_URL + "/tinygray-articles/image"
  },
  menu: {
    getMenuList: GLOBAL_API_URL + "/tinygray-front-menu/selectMenus",
    getMenuById: GLOBAL_API_URL + "/tinygray-front-menu/info",
    saveOrUpdateFrontMenu: GLOBAL_API_URL + "/tinygray-front-menu/info/saveOrUpdate",
    deleteMenu: GLOBAL_API_URL + "/tinygray-front-menu/info/delete"
  },
  code: {
    getCode: GLOBAL_API_URL + "/sms/ali/sendCodeSms"
  },
  // 当然也可以用文件方式进行管理
  Goods: Goods,

};
