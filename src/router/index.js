import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login/Login';
import Index from '../views/backstage/Index'
Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'login',
      component: () => import('@/views/Login/Login'),
      meta:{allowBack: false}
    },
  ]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
