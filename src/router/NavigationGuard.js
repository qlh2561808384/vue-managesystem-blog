import router from './index'
import Auth from "../assets/js/Auth";
import store from '../store/index'
router.beforeEach((to, from, next) => {
    if (to.path === '/') {
        next();
    } else {
        if (Auth.getToken()) {
            //再加入
            store.commit('setMenu',  Auth.getMenus())
            // 防止刷新后vuex里丢失标签列表tagList
            store.commit('getMenu')
            next();
        } else {
            next({name: 'login'});
        }
    }
});
